//
//  WeatherViewModel.swift
//  WheatherApp
//
//  Created by developer on 11/03/22.
//

import Foundation
import MapKit
import SwiftUI
import Combine

class WeatherViewModel: ObservableObject {

    enum State {
        case idle
        case loading
        case failed(String)
        case loaded
    }

    @Published private(set) var cityName = String.empty
    @Published private(set) var temperature = Int.zero
    @Published private(set) var temperatureMax = Int.zero
    @Published private(set) var temperatureMin = Int.zero
    @Published private(set) var description = String.empty
    @Published private(set) var feelsLike = Int.zero
    @Published private(set) var humidity = Int.zero
    @Published var region = MKCoordinateRegion()
    @Published private(set) var state = State.idle

    private var cancellable: AnyCancellable?

    func load() {
        state = .loading
        cancellable?.cancel()
        cancellable = WeatherApiService().fetch(cityName: "Diadema", country: "BR")
            .sink { [weak self] receiveCompletion in
                switch receiveCompletion {
                case .finished:
                    break
                case .failure(let error):
                    self?.onError(message: error.localizedDescription)
                }
            } receiveValue: { [weak self] response in
                self?.onSuccess(response: response)
            }
    }

    private func onError(message: String){
        DispatchQueue.main.async { [weak self] in
            self?.state = .failed(message)
        }
    }
    private func onSuccess(response: WeatherResponse) {
        let center = CLLocationCoordinate2D(latitude: response.coord.lat, longitude: response.coord.lon)
        let span = MKCoordinateSpan(latitudeDelta: 0.5, longitudeDelta: 0.5)

        DispatchQueue.main.async { [weak self] in
            self?.cityName = response.name
            self?.temperature = Int(response.main.temp)
            self?.temperatureMax = Int(response.main.tempMax)
            self?.temperatureMin = Int(response.main.tempMin)
            self?.description = response.weather.first?.description ?? String.empty
            self?.feelsLike = Int(response.main.feelsLike)
            self?.humidity = response.main.humidity
            self?.region = MKCoordinateRegion(center: center, span: span)
            self?.state = .loaded
        }
    }
}

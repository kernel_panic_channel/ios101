//
//  WeatherApiService.swift
//  WheatherApp
//
//  Created by developer on 13/03/22.
//

import Foundation

import Combine

enum NetworkError: Error {
    case unknown
    case request(underlyingError: Error)
    case unableToDecode(underlyingError: Error)
}

class WeatherApiService {

    private struct Constant {
        static let uri = "https://api.openweathermap.org/data/2.5/weather"
        static let query = "q"
        static let fatalErrorMessage = "Invalid URL."
        static let parameters = [
            URLQueryItem(name: "units", value: "metric"),
            URLQueryItem(name: "lang", value: "pt"),
            URLQueryItem(name: "appid", value: "ded26a6c4cc74fe62ee6ab7c8e8068dd")
        ]
    }

    private var urlComponents = URLComponents(string: Constant.uri)
    private var decoder: JSONDecoder {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }

    func fetch(cityName: String, country: String) -> AnyPublisher<WeatherResponse, NetworkError> {

        urlComponents?.queryItems = [URLQueryItem]()
        urlComponents?.queryItems?.append(URLQueryItem(name: Constant.query, value: "\(cityName),\(country)"))
        urlComponents?.queryItems?.append(contentsOf: Constant.parameters)

        guard let url = urlComponents?.url else { fatalError(Constant.fatalErrorMessage) }

        let request = URLRequest(url: url)

        return URLSession.DataTaskPublisher(request: request, session: .shared)
            .mapError { NetworkError.request(underlyingError: $0) }
            .tryMap({ (data, response) -> Data in
                if let response = response as? HTTPURLResponse,
                   (200..<300).contains(response.statusCode) == false {
                    throw NetworkError.unknown
                }
                return data
            })
            .decode(type: WeatherResponse.self, decoder: decoder)
            .mapError { $0 as? NetworkError ?? .unableToDecode(underlyingError: $0) }
            .eraseToAnyPublisher()
    }
}

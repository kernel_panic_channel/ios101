//
//  Fonts.swift
//  WheatherApp
//
//  Created by developer on 08/03/22.
//

import SwiftUI

extension Font {

    static var helveticaNeueUltraLight48: Font {
        Font.custom("HelveticaNeue-Ultralight", size: 48)
    }

    static var helveticaNeueRegular12: Font {
        Font.custom("HelveticaNeue", size: 12)
    }

    static var helveticaNeueThin96: Font {
        Font.custom("HelveticaNeue-Thin", size: 96)
    }

    static var helveticaNeueBold15: Font {
        Font.custom("HelveticaNeue-Bold", size: 15)
    }
}

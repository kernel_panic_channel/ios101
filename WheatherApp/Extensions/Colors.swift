//
//  Colors.swift
//  WheatherApp
//
//  Created by developer on 08/03/22.
//

import SwiftUI

extension Color {

    static var darkBlue: Color {
        Color(red: 14/255, green: 0, blue: 1)
    }

    static var lightBlue: Color {
        Color(red: 0, green: 146/255, blue: 1)
    }

    static var boderLightBlue: Color {
        Color(red: 0, green: 121/255, blue: 1)
    }
}

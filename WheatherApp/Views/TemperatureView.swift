//
//  TemperatureView.swift
//  WheatherApp
//
//  Created by developer on 08/03/22.
//

import SwiftUI

struct TemperatureView: View {
    
    private let temperature: Int
    private let temperatureMax: Int
    private let temperatureMin: Int
    private let description: String
    private let feelsLike: Int
    private let humidity: Int
    
    private struct Constant {
        static let textColor = Color.white
        static let degreeSymbol = "° C"
        static let humiditySymbol = "%"
        static let topPaddingDistance = 45.0
        static let feelsLikeDescription = "Sensação térmica: "
        static let humidityDescription = "Humidade: "
        static let maxTempDescription = "Max: "
        static let minTempDescription = "Min: "
    }
    
    init(temperature: Int, temperatureMax: Int, temperatureMin: Int, description: String, feelsLike: Int, humidity: Int) {
        self.temperature = temperature
        self.temperatureMax = temperatureMax
        self.temperatureMin = temperatureMin
        self.description = description
        self.feelsLike = feelsLike
        self.humidity = humidity
    }
    
    var body: some View {
        HStack(alignment: .top) {
            VStack(alignment: .leading) {
                Text("\(temperature)\(Constant.degreeSymbol)")
                    .font(.helveticaNeueThin96)
                    .foregroundColor(Constant.textColor)
                Text(description)
                    .font(.helveticaNeueBold15)
                    .foregroundColor(Constant.textColor)
                VStack(alignment: .leading) {
                    Text("\(Constant.feelsLikeDescription)\(feelsLike)\(Constant.degreeSymbol)")
                        .font(.helveticaNeueRegular12)
                        .foregroundColor(Constant.textColor)
                    Text("\(Constant.humidityDescription)\(humidity)\(Constant.humiditySymbol)")
                        .font(.helveticaNeueRegular12)
                        .foregroundColor(Constant.textColor)
                }
            }
            VStack(alignment: .leading) {
                Text("\(Constant.maxTempDescription)\(temperatureMax)\(Constant.degreeSymbol)")
                    .font(.helveticaNeueRegular12)
                    .foregroundColor(Constant.textColor)
                Text("\(Constant.minTempDescription)\(temperatureMin)\(Constant.degreeSymbol)")
                    .font(.helveticaNeueRegular12)
                    .foregroundColor(Constant.textColor)
            }.padding([.top], Constant.topPaddingDistance)
        }
    }
}

struct TemperatureView_Previews: PreviewProvider {
    static var previews: some View {
        TemperatureView(temperature: 1, temperatureMax: 1, temperatureMin: 1, description: "Cloudy", feelsLike: 1, humidity: 1)
    }
}

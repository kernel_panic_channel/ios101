//
//  MapView.swift
//  WheatherApp
//
//  Created by developer on 11/03/22.
//

import SwiftUI
import MapKit

struct MapView: View {

    @ObservedObject private var viewModel = WeatherViewModel()

    private struct Constant {
        static let cornerRadius = 10.0
        static let lineWidth = 10.0
        static let padding = 30.0
        static let mapHeight = 300.0
    }

    init(viewModel: WeatherViewModel) {
        self.viewModel = viewModel
    }

    var body: some View {

        Map(coordinateRegion: $viewModel.region)
            .frame(height: Constant.mapHeight)
            .overlay(
                RoundedRectangle(cornerRadius: Constant.cornerRadius)
                    .stroke(Color.boderLightBlue, lineWidth: Constant.lineWidth)
            )
            .padding(.all, Constant.padding)
    }
}

struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView(viewModel: WeatherViewModel())
    }
}

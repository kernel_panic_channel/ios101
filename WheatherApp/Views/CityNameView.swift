//
//  CityDisplay.swift
//  WheatherApp
//
//  Created by developer on 08/03/22.
//

import SwiftUI

struct CityNameView: View {
    
    private let name: String
    
    init(name: String) {
        self.name = name
    }
    
    var body: some View {
        Text(name)
            .font(.helveticaNeueUltraLight48)
            .foregroundColor(Color.white)
    }
}

struct CityDisplay_Previews: PreviewProvider {
    static var previews: some View {
        CityNameView(name: "London")
    }
}

//
//  ContentView.swift
//  WheatherApp
//
//  Created by developer on 08/03/22.
//

import SwiftUI
import MapKit
import SwiftfulLoadingIndicators

struct MainContentView: View {

    @ObservedObject var viewModel = WeatherViewModel()

    var body: some View {

        switch viewModel.state {
        case .idle:
            Color.clear.onAppear(perform: viewModel.load)
        case .loading:
            ZStack {
                GradientView()
                LoadingIndicator(animation: .fiveLines, color: .white, size: .large)
            }
        case .failed(let error):
            Text(error)
        case .loaded:
            ZStack {
                GradientView()
                VStack{
                    CityNameView(name: viewModel.cityName)
                    TemperatureView(temperature: viewModel.temperature,
                                    temperatureMax: viewModel.temperatureMax,
                                    temperatureMin: viewModel.temperatureMin,
                                    description: viewModel.description,
                                    feelsLike: viewModel.feelsLike,
                                    humidity: viewModel.humidity)
                    MapView(viewModel: viewModel)
                }
            }
        }


    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainContentView()
    }
}

//
//  WeatherResponse.swift
//  WheatherApp
//
//  Created by developer on 11/03/22.
//

import Foundation

struct WeatherResponse: Decodable {

    struct Coordinate: Decodable {
        let lat: Double
        let lon: Double
    }

    struct Info: Decodable {
        let description: String
    }

    struct Temperature: Decodable {
        let temp: Double
        let feelsLike: Double
        let tempMin: Double
        let tempMax: Double
        let humidity: Int
    }

    let coord: Coordinate
    let weather: [Info]
    let main: Temperature
    let name: String
}

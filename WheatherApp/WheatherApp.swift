//
//  WheatherAppApp.swift
//  WheatherApp
//
//  Created by developer on 08/03/22.
//

import SwiftUI

@main
struct WheatherApp: App {
    var body: some Scene {
        WindowGroup {
            MainContentView()
        }
    }
}
